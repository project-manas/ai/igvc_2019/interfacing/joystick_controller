/*
    Created on: 19-Sep-2018
    Author: naiveHobo
*/

#include <ros/ros.h>

#include <std_msgs/Bool.h>
#include <geometry_msgs/Twist.h>

#include "joystick.hh"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory>

class JoystickController {

 private:

  enum class ButtonMap {
    BRAKE = 1,          // B
    TRACKING = 2,       // X
    SAFE_MODE = 5       // RB
  };

  enum class AxisMap {
    X = 0,        // LEFT ANALOG
    Y = 1,        // LEFT ANALOG
  };

  ros::NodeHandle nh_;

  ros::NodeHandle private_nh_;
  ros::Publisher cmd_pub_;
  ros::Publisher tracking_pub_;

  std::shared_ptr<Joystick> joystick_;

  double linear_velocity_;
  double angular_velocity_;

  double max_linear_velocity_;
  double max_angular_velocity_;

  bool is_tracking_on_;
  bool safe_mode_;

  std::string cmd_topic_;
  std::string tracking_topic_;

 public:

  JoystickController() : private_nh_("~") {

    int dev;
    private_nh_.param("device_id", dev, 0);
    joystick_.reset(new Joystick(dev));

    if (!this->joystick_->isFound()) {
      ROS_ERROR("Could not find JoyStick device");
      exit(1);
    } else {
      ROS_INFO("Found JoyStick at device with id [%d]", dev);
    }

    linear_velocity_ = 0.0;
    angular_velocity_ = 0.0;
    max_linear_velocity_ = 0.5;
    max_angular_velocity_ = 0.5;
    is_tracking_on_ = false;
    safe_mode_ = true;

    private_nh_.param("max_linear_velocity", max_linear_velocity_, 0.5);
    private_nh_.param("max_angular_velocity", max_angular_velocity_, 0.5);
    private_nh_.param<std::string>("cmd_vel_topic", cmd_topic_, "cmd_vel");
    private_nh_.param<std::string>("tracking_topic", tracking_topic_, "start_tracking");

    cmd_pub_ = nh_.advertise<geometry_msgs::Twist>(cmd_topic_, 1);
    tracking_pub_ = nh_.advertise<std_msgs::Bool>(tracking_topic_, 1);
  }

  void run() {

    while (ros::ok()) {

      usleep(1000);

      JoystickEvent event;

      if (joystick_->sample(&event)) {
        eventControl(event);
      }

      publishCommand();

      ros::spinOnce();
    }
  }

  void eventControl(JoystickEvent event) {

    if (event.isButton()) {

      // ROS_INFO("Button %u is %s", event.number, event.value == 0 ? "up" : "down");

      auto event_num = static_cast<ButtonMap>(event.number);

      switch (event_num) {
        case ButtonMap::BRAKE: {
          if (event.value != 0) {
            linear_velocity_ = 0.0;
            angular_velocity_ = 0.0;
            ROS_INFO("Applying brakes!");
          }
          break;
        }
        case ButtonMap::TRACKING: {
          std_msgs::Bool msg;
          if (event.value != 0) {
            is_tracking_on_ = !is_tracking_on_;
            msg.data = (u_char) is_tracking_on_;
            tracking_pub_.publish(msg);
            ROS_INFO("Switched %s person tracking!", is_tracking_on_ ? "on" : "off");
          }
          break;
        }
        case ButtonMap::SAFE_MODE: {
          if (event.value != 0) {
            safe_mode_ = !safe_mode_;
            if (!safe_mode_) {
              max_linear_velocity_ *= 2.0;
              max_linear_velocity_ *= 2.0;
              ROS_INFO("Danger Mode!");
            } else {
              max_linear_velocity_ /= 2.0;
              max_linear_velocity_ /= 2.0;
              ROS_INFO("Safe Mode!");
            }
          }
          break;
        }
      }

    } else if (event.isAxis()) {

      auto event_num = static_cast<AxisMap>(event.number);

      switch (event_num) {
        case AxisMap::X: {
          angular_velocity_ = -event.value / 32767.0;
          break;
        }
        case AxisMap::Y: {
          linear_velocity_ = -event.value / 32767.0;
          break;
        }
      }
    }
  }

  void publishCommand() {
    geometry_msgs::Twist cmd;
    cmd.linear.x = linear_velocity_ * max_linear_velocity_;
    cmd.angular.z = angular_velocity_ * max_angular_velocity_;
    cmd_pub_.publish(cmd);
  }

};

int main(int argc, char **argv) {
  ros::init(argc, argv, "joystick_controller");

  JoystickController control;
  control.run();

  return 0;
}
