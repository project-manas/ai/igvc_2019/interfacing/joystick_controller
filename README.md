# Joystick Controller

ROS interface for USB JoyStick to send velocity commands.

### Usage
Set the following parameters in `joystick.launch`:
  - `device_id (default: 0)`: ID of USB device where joystick is connected
  - `cmd_vel_topic (defualt: "/cmd_vel")`: Topic where velocity commands are to be published (type: *geometry_msgs/Twist*)
  - `tracking_topic (default: "/start_tracking")`: Topic where commands for person tracking are to be published (type: *std_msgs/Bool*)
  - `max_linear_velocity (default: 0.5)`: Maximum linear velocity (m/s)
  - `max_angular_velocity (default: 0.5)`: Maximum angular velocity (rad/s)
  
To launch the joystick controller node:
```bash
roslaunch joystick_controller joystick.launch
```
